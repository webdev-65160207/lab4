type Gender = 'male' | 'female' | 'other'
type Roles = 'admin' | 'user'
type User = {
    id: number
    email: string
    password: string
    fullname: string
    gender: Gender
    roles: Roles[]
}

export type {Gender, Roles, User}